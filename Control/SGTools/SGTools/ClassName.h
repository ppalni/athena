// This file's extension implies that it's C, but it's really -*- C++ -*-.

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: ClassName.h,v 1.1 2009-01-15 19:07:29 binet Exp $
/**
 * @file  SGTools/ClassName.h
 * @author scott snyder
 * @date Jul 2005
 * @brief An interface for getting the name of a class as a string.
 */


#ifndef SGTOOLS_CLASSNAME_H
#define SGTOOLS_CLASSNAME_H


#include "AthenaKernel/ClassName.h"


#endif // not SGTOOLS_CLASSNAME_H
